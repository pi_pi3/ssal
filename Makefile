STDDIR:=$(HOME)/.local/lib64/etc/std
SRCDIR:=src/
SRC:=$(shell find src/ -name '*.amp')
STD:=$(shell find $(HOME)/.local/lib64/etc/std/ -name '*.amp')
OBJ:=$(patsubst src/%.amp,obj/%.amp.o,$(SRC)) \
	 $(patsubst $(STDDIR)/%.amp,obj/std/%.amp.o,$(STD))
BIN:=main

AMPFLAGS=--system-c -I $(HOME)/.local/lib64/etc/ --package ssal
LDFLAGS=-lm

ifeq ($(VERBOSE),1)
	AMPFLAGS+=-vvv
endif

ifeq ($(RELEASE),1)
	AMPFLAGS+=-O3
endif

AMPC=etc
LD=clang

.PHONY: all run install uninstall clean mrproper

all: $(BIN)

run: $(BIN)
	./$(BIN)

obj/%.amp.o: src/%.amp
	$(AMPC) $(AMPFLAGS) -c -o $@ $^

obj/std/%.amp.o: $(STDDIR)/%.amp
	$(AMPC) $(AMPFLAGS) -c -o $@ $^

$(BIN): $(OBJ)
	$(LD) $(LDFLAGS) -o $@ $^

install:
	mkdir -p ~/.local/lib64/etc
	rm -rf ~/.local/lib64/etc/ssal
	cp -r $(SRCDIR) ~/.local/lib64/etc/ssal
	@rm ~/.local/lib64/etc/ssal/main.amp

uninstall:
	rm -rf ~/.local/lib64/etc/ssal

clean:
	rm -f $(OBJ)

mrproper: clean
	rm -f $(BIN)
