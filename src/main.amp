mod ssal;

use std::collections::option;
use extern "C" "stdlib.h";
use ssal::lib;
use ssal::ctx;
use ssal::module;
use ssal::proc;
use ssal::basic_block;
use ssal::instruction;
use ssal::value;

with __lang::builtin;
with std::collections::option;
with ssal;
with ssal::ctx;
with ssal::module;
with ssal::proc;
with ssal::basic_block;
with ssal::instruction;
with ssal::value;

Ref<Value> print(Ref<Context> ctx, Ref<Module> module, Ref<Thread> thread, Ref<Value>[]* args, Ref<void> userdata) {
    let args = *args;
    for (let i = 0; i < args.len; i++) {
        if (i > 0) {
            write(stdout, " ");
        }
        write(stdout, args[i]);
    }
    return add_value(ctx);
}

Ref<Value> println(Ref<Context> ctx, Ref<Module> module, Ref<Thread> thread, Ref<Value>[]* args, Ref<void> userdata) {
    let result = print(ctx, module, thread, args, userdata);
    writeln(stdout, "");
    return result;
}

void dump_location(Ref<Module> module, Ref<BasicBlock> bb, String directory, String file, uint line, uint col) {
    Metadata md;
    new(&md, MetadataKind::SourceLocation {
        .directory = directory,
        .file = file,
        .line = line,
        .col = col,
    });
    let md = add_metadata(module, md);
    add_metadata(bb, md);
}

/*
 *  fn proc(x) {
 *      i := 0;
 *      while (i < x) {
 *          i = i + 1
 *      }
 *      return i;
 *  }
 *
 *  fn main() {
 *      result := 10000 + 20000;
 *      thunk := defer proc(result);
 *      result := join thunk;
 *      return system.println();
 *  }
 */

sint main(String[] args) {
    Context ctx;
    new(&ctx);

    let ctx_ref = as_ref(&ctx);

    let module = add_module(ctx_ref, "main");
    let thread = add_thread(module, "root");

    let proc = add_symbol(ctx_ref, "proc");

    let x = add_symbol(ctx_ref, "x");
    Array<Ref<Symbol>> params;
    new(&params);
    push(&params, x);
    
    let proc = add_procedure(module, proc, params);
    let start = add_basic_block(proc, add_symbol(ctx_ref, ""));
    let bb0_name = add_symbol(ctx_ref, "");
    let bb1_name = add_symbol(ctx_ref, "");
    let bb0 = add_basic_block(proc, bb0_name);
    let bb1 = add_basic_block(proc, bb1_name);
    let i_name = add_symbol(ctx_ref, "i");
    build_alloca(start, add_value(ctx_ref, 0), i_name);
    dump_location(module, start, "./", "test.asc", 2, 4);
    let i = add_value(ctx_ref, i_name);
    build_br(start, false, add_value(ctx_ref, bb0));
    dump_location(module, start, "./", "test.asc", 3, 4);
    let tmp1_name = add_symbol(ctx_ref, "");
    build_enter(bb0);
    dump_location(module, bb0, "./", "test.asc", 3, 12);
    build_add(bb0, i, add_value(ctx_ref, 1), tmp1_name);
    dump_location(module, bb0, "./", "test.asc", 4, 13);
    let tmp1 = add_value(ctx_ref, tmp1_name);
    build_store(bb0, tmp1, i);
    dump_location(module, bb0, "./", "test.asc", 4, 8);
    let cmp_name = add_symbol(ctx_ref, "");
    build_cmp(bb0, Cmp::LT, i, add_value(ctx_ref, x), cmp_name);
    dump_location(module, bb0, "./", "test.asc", 3, 12);
    let cmp = add_value(ctx_ref, cmp_name);
    build_br(bb0, true, cmp, add_value(ctx_ref, bb0), add_value(ctx_ref, bb1));
    dump_location(module, bb0, "./", "test.asc", 5, 4);
    build_ret(bb1, i);
    dump_location(module, bb1, "./", "test.asc", 6, 4);

    let main = add_symbol(ctx_ref, "main");
    Array<Ref<Symbol>> params;
    new(&params);
    let main = add_procedure(module, main, params);
    let bb = add_basic_block(main, add_symbol(ctx_ref, ""));
    let result = add_symbol(ctx_ref, "result");

    build_add(bb, add_value(ctx_ref, 10000), add_value(ctx_ref, 20000), result);
    dump_location(module, bb, "./", "test.asc", 10, 4);
    Array<Ref<Value>> args;
    new(&args);
    push(&args, add_value(ctx_ref, result));
    let app = add_symbol(ctx_ref, "thunk");
    build_defer(bb, add_value(ctx_ref, proc), args, app);
    dump_location(module, bb, "./", "test.asc", 11, 4);
    let join = add_symbol(ctx_ref, "result");
    build_join(bb, add_value(ctx_ref, app), join);
    dump_location(module, bb, "./", "test.asc", 12, 4);
    Array<Ref<Value>> args;
    new(&args);
    push(&args, add_value(ctx_ref, join));
    let app = add_symbol(ctx_ref, "");
    build_call(bb, add_value(ctx_ref, as_ref(cast<void*>(0u_t)), println), args, app);
    dump_location(module, bb, "./", "test.asc", 13, 11);
    build_ret(bb, add_value(ctx_ref, app));
    dump_location(module, bb, "./", "test.asc", 13, 4);

    writeln(stdout, module);

    call(thread, main);
    match (run(ctx_ref, module)) {
        Option<Ref<Value>>::Some { .some } => {
            writeln(stdout, some);
        }
    }

    del(ctx);
    return 0;
}

attr((no_mangle))
sint main(sint argc, schar** argv) {
    String[] args;
    args.len = cast<usizet>(argc);
    args.ptr = cast<String*>(malloc(cast<size_t>(cast<usizet>(argc) * sizeof(String))));
    for (let i = 0; i < cast<usizet>(argc); i++) {
        args[i].ptr = argv[i];
        args[i].len = cast<usizet>(strlen(argv[i]));
    }

    let result = main(args);

    free(cast<void*>(args.ptr));

    return result;
}
